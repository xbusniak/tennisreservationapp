package com.example.TennisReservation.model;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * Player class.
 * This class represents Tennis player in database.
 *
 * @author Lucia Busniakova
 */
@Entity
@Table(name = "players")
@SQLDelete(sql = "UPDATE players SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String telephoneNumber;

    @Column
    private String playerName;

    private boolean deleted = Boolean.FALSE;

    public Player() {

    }

    public Player(String telephoneNumber, String playerName) {
        this.playerName = playerName;
        this.telephoneNumber = telephoneNumber;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public long getId() {
        return this.id;
    }

    public String getTelephoneNumber() {
        return this.telephoneNumber;
    }

    public String getPlayerName() {
        return this.playerName;
    }
}
