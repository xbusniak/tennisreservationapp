package com.example.TennisReservation.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Reservation class.
 * This class represents reservations.
 *
 * @author Lucia Busniakova
 */
@Entity
@Table(name = "reservations")
@SQLDelete(sql = "UPDATE reservations SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class Reservation {

    @Id
    @GeneratedValue
    private long id;
    private String telephoneNumber;
    private String playerName;
    private int courtNumber;
    private long timeMinutes;
    private boolean deleted = Boolean.FALSE;

    @Enumerated(EnumType.STRING)
    private GameType gameType;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate reservationDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private LocalTime reservationTime;

    public void setId(long id) {
        this.id = id;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void setCourtNumber(int courtNumber) {
        this.courtNumber = courtNumber;
    }

    public void setTimeMinutes(long timeMinutes) {
        this.timeMinutes = timeMinutes;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }

    public void setReservationDate(LocalDate reservationDate) {
        this.reservationDate = reservationDate;
    }

    public void setReservationTime(LocalTime reservationTime) {
        this.reservationTime = reservationTime;
    }

    public long getId() {
        return this.id;
    }

    public String getTelephoneNumber() {
        return this.telephoneNumber;
    }

    public String getPlayerName() {
        return this.playerName;
    }

    public int getCourtNumber() {
        return this.courtNumber;
    }

    public long getTimeMinutes() {
        return this.timeMinutes;
    }

    public GameType getGameType() {
        return this.gameType;
    }

    public LocalDate getReservationDate() {
        return this.reservationDate;
    }

    public LocalTime getReservationTime() {
        return this.reservationTime;
    }
}
