package com.example.TennisReservation.model;

/**
 * Enum class which represents
 * how many players will be playing the game.
 *
 * @author Lucia Busniakova
 */

public enum GameType {
    TWO_PLAYERS,
    FOUR_PLAYERS
}
