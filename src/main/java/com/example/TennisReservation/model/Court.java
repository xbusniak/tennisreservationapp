package com.example.TennisReservation.model;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * Court class.
 * This class represents tennis court.
 *
 * @author Lucia Busniakova
 */

@Entity
@Table(name = "courts")
@SQLDelete(sql = "UPDATE courts SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class Court {

    @Id
    @GeneratedValue
    private long id;

    @Enumerated(EnumType.STRING)
    private SurfaceType surfaceType;

    private int courtNumber;

    private boolean deleted = Boolean.FALSE;

    public void setId(long id) {
        this.id = id;
    }

    public void setSurfaceType(SurfaceType surfaceType) {
        this.surfaceType = surfaceType;
    }

    public void setCourtNumber(int courtNumber) {
        this.courtNumber = courtNumber;
    }

    public long getId() {
        return this.id;
    }

    public SurfaceType getSurfaceType() {
        return this.surfaceType;
    }

    public int getCourtNumber() {
        return this.courtNumber;
    }
}
