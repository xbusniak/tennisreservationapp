package com.example.TennisReservation.model;

/**
 * Enum SurfaceType.
 * This enum class represents types of court surfaces
 * with the price for 1 minute.
 */
public enum SurfaceType {
    GRASS(10),
    HARD(5),
    CLAY(6);

    private final int price;

    SurfaceType(int price) {
        this.price = price;
    }

    public int getPrice() {
        return this.price;
    }

}
