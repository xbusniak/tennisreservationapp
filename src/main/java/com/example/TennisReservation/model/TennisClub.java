package com.example.TennisReservation.model;

import java.time.LocalTime;

/**
 * TennisClub class.
 * This class represents the tennis club.
 *
 * @author Lucia Busniakova
 */

public class TennisClub {

    private static final LocalTime clubOpening = LocalTime.of(8, 0);
    private static final LocalTime clubClosing = LocalTime.of(22, 0);

    private static int numberOfCourts = 0;

    public static LocalTime getClubOpening() {
        return clubOpening;
    }

    public static LocalTime getClubClosing() {
        return clubClosing;
    }

    public static void increaseNumberOfCourts() {
        TennisClub.numberOfCourts += 1;
    }
}
