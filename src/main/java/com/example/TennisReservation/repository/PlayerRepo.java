package com.example.TennisReservation.repository;

import com.example.TennisReservation.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * PlayerRepo interface.
 * Interface which is responsible for manipulation with customer database.
 *
 * @author Lucia Busniakova
 */

@Repository
public interface PlayerRepo extends JpaRepository<Player, Long> {

    /**
     * Get player by given telephone number.
     *
     * @param telephoneNumber identification of each player
     * @return player with given telephone number
     */
    @Query(value = "select * from player player where player.telephoneNumber = ?1", nativeQuery = true)
    Player getPlayerByTelephoneNumber(String telephoneNumber);
}
