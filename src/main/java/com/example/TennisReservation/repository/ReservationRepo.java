package com.example.TennisReservation.repository;

import com.example.TennisReservation.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ReservationRepo interface.
 * Interface which is responsible for manipulation with reservation database.
 *
 * @author Lucia Busniakova
 */

@Repository
public interface ReservationRepo extends JpaRepository<Reservation, Long> {
    /**
     * Gets reservations for a given court.
     *
     * @param courtNumber identification number of the court
     * @return list of all reservations on a given court
     */
    @Query(value = "select * from reservation res where res.courtNumber = ?1", nativeQuery = true)
    List<Reservation> getReservationsByCourtNumber(Integer courtNumber);

    /**
     * Gets all the reservations of one player with given phone number.
     *
     * @param telephoneNumber identification telephone number of the player
     * @return list of all reservations of a given telephone number
     */
    @Query(value = "select * from reservation res where res.telephoneNumber = ?1", nativeQuery = true)
    List<Reservation> getReservationsByTelephoneNumber(String telephoneNumber);

    /**
     * Gets all the reservations of one player with given phone number in the future.
     *
     * @param telephoneNumber identification telephone number of the player
     * @return list of all reservations of a given telephone number in the future
     */
    @Query(value = "select * from reservation res where res.telephoneNumber = ?1 and res.reservationDate > current_date and res.reservationTime > current_time()", nativeQuery = true)
    List<Reservation> getReservationsByTelephoneNumberAndReservationDateTime(String telephoneNumber);

    /**
     * Gets all the reservations for given court on a given date.
     *
     * @param courtNumber represents court
     * @param date represents date
     * @return list of all reservations of a given date and court
     */
    @Query(value = "select * from reservation res where res.reservationDate = ?1 and res.courtNumber = ?2", nativeQuery = true)
    List<Reservation> getReservationsByCourtNumberAndReservationDate(Integer courtNumber, String date);
}
