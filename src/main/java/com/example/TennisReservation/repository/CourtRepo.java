package com.example.TennisReservation.repository;

import com.example.TennisReservation.model.Court;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * CourtRepo interface.
 * Interface which is responsible for manipulation with court database.
 *
 * @author Lucia Busniakova
 */

public interface CourtRepo extends JpaRepository<Court, Long> {

    /**
     * Gets court which is represented by given courtNumber.
     *
     * @param courtNumber represents number of the court
     * @return court which is represented by given courtNumber
     */
    @Query(value = "SELECT * FROM court court WHERE court.courtNumber = ?1", nativeQuery = true)
    Court getCourtByCourtNumber(Integer courtNumber);

}
