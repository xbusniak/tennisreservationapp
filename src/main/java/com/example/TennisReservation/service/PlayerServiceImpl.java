package com.example.TennisReservation.service;

import com.example.TennisReservation.model.Player;
import com.example.TennisReservation.repository.PlayerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    private PlayerRepo playerRepo;

    /**
     * Pattern for telephone number.
     * This pattern will allow numbers like 776524281, 776 524 281, 776-524-281.
     */
    private static final String TELEPHONE_NUMBER_FORMAT = "^(\\d{3}[- ]?){2}\\d{3}$";

    @Override
    public void createPlayer(Player player) {
        validatePlayer(player);
        playerRepo.save(player);
    }

    @Override
    public void updatePlayer(long id, Player player) {
        if (!playerRepo.findById(id).isPresent()) {
            throw new IllegalArgumentException("Player with given id doesn't exist.");
        }

        validatePlayer(player);

        Player updatedPlayer = playerRepo.findById(id).get();
        updatedPlayer.setPlayerName(player.getPlayerName());
        updatedPlayer.setTelephoneNumber(player.getTelephoneNumber());

        playerRepo.save(updatedPlayer);
    }

    @Override
    public void deletePlayer(long id) {
        playerRepo.deleteById(id);
    }

    @Override
    public List<Player> getAllPlayers() {
        return playerRepo.findAll();
    }

    private void validatePlayer(Player player) {
        if (player.getPlayerName() == null || Objects.equals(player.getPlayerName(), "")) {
            throw new IllegalArgumentException("Player name cannot be empty!");
        }

        if (!player.getTelephoneNumber().matches(TELEPHONE_NUMBER_FORMAT)) {
            throw new IllegalArgumentException("Invalid format of the given telephone number!");
        }
    }
}
