package com.example.TennisReservation.service;

import com.example.TennisReservation.model.Court;
import com.example.TennisReservation.repository.CourtRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
public class CourtServiceImpl implements CourtService {

    @Autowired
    private CourtRepo courtRepo;

    public List<Court> getAllCourts() {
        return courtRepo.findAll();
    }

    public void createCourt(@RequestBody Court court) {
        courtRepo.save(court);
    }

    public void updateCourt(@PathVariable long id,
                              @RequestBody Court court) {

        if (!courtRepo.findById(id).isPresent()) {
            throw new IllegalArgumentException("Court with given id doesn't exist.");

        }

        Court updatedCourt = courtRepo.findById(id).get();
        updatedCourt.setSurfaceType(court.getSurfaceType());
        courtRepo.save(updatedCourt);
    }

    public void deleteCourt(@PathVariable long id) {
        courtRepo.deleteById(id);
    }
}
