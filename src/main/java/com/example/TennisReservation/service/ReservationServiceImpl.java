package com.example.TennisReservation.service;

import com.example.TennisReservation.model.Reservation;
import com.example.TennisReservation.model.Player;
import com.example.TennisReservation.model.Court;
import com.example.TennisReservation.model.GameType;
import com.example.TennisReservation.model.TennisClub;
import com.example.TennisReservation.repository.CourtRepo;
import com.example.TennisReservation.repository.PlayerRepo;
import com.example.TennisReservation.repository.ReservationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationRepo reservationRepo;

    /**
     * Pattern for telephone number.
     * This pattern will allow numbers like 776524281, 776 524 281, 776-524-281.
     */
    private static final String TELEPHONE_NUMBER_FORMAT = "^(\\d{3}[- ]?){2}\\d{3}$";

    /**
     * Represents tax for game with four players.
     */
    private static final double DOUBLE_TAX = 1.5;
    @Autowired
    private CourtRepo courtRepo;
    @Autowired
    private PlayerRepo playerRepo;

    @Override
    public List<Reservation> getReservations() {
        return reservationRepo.findAll();
    }

    @Override
    public List<Reservation> getReservationsByCourtNumber(int courtNumber) {
        return reservationRepo.getReservationsByCourtNumber(courtNumber);
    }

    @Override
    public List<Reservation> getReservationByTelephoneNumber(String telephoneNumber) {
        return reservationRepo.getReservationsByTelephoneNumber(telephoneNumber);
    }

    @Override
    public List<Reservation> getReservationsByTelephoneNumberAndReservationDateTime(String telephoneNumber) {
        return reservationRepo.getReservationsByTelephoneNumberAndReservationDateTime(telephoneNumber);
    }

    @Override
    public void deleteReservation(long id) {
        reservationRepo.deleteById(id);
    }

    @Override
    public double createReservation(Reservation reservation) {

        validateReservation(reservation);
        checkReservationCollisions(reservation);

        Player player = playerRepo.getPlayerByTelephoneNumber(reservation.getTelephoneNumber());
        if (player == null) {
            Player newPlayer = new Player(reservation.getTelephoneNumber(), reservation.getPlayerName());
            playerRepo.save(newPlayer);
        }

        reservationRepo.save(reservation);
        return getReservationPrice(reservation);
    }

    @Override
    public double updateReservation(long id, Reservation reservation) {
        if (!reservationRepo.findById(id).isPresent()) {
            throw new IllegalArgumentException("Reservation with given id doesn't exist.");
        }

        validateReservation(reservation);
        checkReservationCollisions(reservation);

        Reservation updateReservation = reservationRepo.findById(id).get();
        updateReservation.setReservationTime(reservation.getReservationTime());
        updateReservation.setReservationDate(reservation.getReservationDate());
        updateReservation.setTelephoneNumber(reservation.getTelephoneNumber());
        updateReservation.setCourtNumber(reservation.getCourtNumber());
        updateReservation.setTimeMinutes(reservation.getTimeMinutes());
        updateReservation.setPlayerName(reservation.getPlayerName());
        updateReservation.setGameType(reservation.getGameType());

        reservationRepo.save(updateReservation);

        return getReservationPrice(reservation);
    }

    /**
     * Calculates price for reservation on given court.
     *
     * @param reservation represents which reservation price we are calculates
     * @return price for given reservation
     */
    private double getReservationPrice(Reservation reservation) {
        double price;
        Court court = courtRepo.getCourtByCourtNumber(reservation.getCourtNumber());
        double priceForSurface = court.getSurfaceType().getPrice();
        price = priceForSurface * reservation.getTimeMinutes();

        if (reservation.getGameType() == GameType.FOUR_PLAYERS) {
            price *= DOUBLE_TAX;
        }
        return price;

    }


    /**
     * Validates attributes of given reservation.
     * @param reservation represents reservation to be validated
     */
    private void validateReservation(Reservation reservation) {
        if (!reservation.getTelephoneNumber().matches(TELEPHONE_NUMBER_FORMAT)) {
            throw new IllegalArgumentException("Invalid format of the given telephone number!");
        }

        if (reservation.getPlayerName().isEmpty()) {
            throw new IllegalArgumentException("Player name cannot be empty!");
        }

        if (reservation.getTimeMinutes() < 30) {
            throw new IllegalArgumentException("Game duration cannot be less than 30 minutes!");
        }

        if (reservation.getTimeMinutes() % 15 != 0) {
            throw new IllegalArgumentException("Reservation must be in normal time duration!");
        }

        if (reservation.getReservationDate().equals(LocalDate.now()) && reservation.getReservationTime().isBefore(LocalTime.now())) {
            throw new IllegalArgumentException("Please select date and time in the future!");
        }

        if (reservation.getReservationTime().isBefore(TennisClub.getClubOpening())) {
            throw new IllegalArgumentException("Reservation cannot be before Tennis club is opening");
        }

        if (reservation.getReservationTime().plusMinutes(reservation.getTimeMinutes()).isAfter(TennisClub.getClubClosing())) {
            throw new IllegalArgumentException("Reservation cannot be after closing Tennis club");
        }

    }

    /**
     * Checks if new reservation has some collisions with existing reservations on court.
     *
     * @param newReservation represents new created reservation
     */
    private void checkReservationCollisions(Reservation newReservation) {
        boolean collision = false;

        String reservationDate = newReservation.getReservationDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        List<Reservation> reservations = reservationRepo.getReservationsByCourtNumberAndReservationDate(newReservation.getCourtNumber(), reservationDate);

        LocalTime startTimeNew = newReservation.getReservationTime();
        LocalTime endTimeNew = startTimeNew.plusMinutes(newReservation.getTimeMinutes());

        for (Reservation reservation : reservations) {
            LocalTime reservationStartTime = reservation.getReservationTime();
            LocalTime reservationEndTime = reservationStartTime.plusMinutes(reservation.getTimeMinutes());

            if (startTimeNew == reservationStartTime || endTimeNew == reservationEndTime) {
                collision = true;
            }
            else if (startTimeNew.isBefore(reservationStartTime) && endTimeNew.isAfter(reservationStartTime)) {
                collision = true;
            }
            else if (startTimeNew.isBefore(reservationEndTime) && endTimeNew.isAfter(reservationEndTime)) {
                collision = true;
            }

            if (!collision) {
                throw new IllegalArgumentException("Reservation with given time cannot be created, " +
                        "because it is in collision with another reservation!");
            }
        }

    }

}
