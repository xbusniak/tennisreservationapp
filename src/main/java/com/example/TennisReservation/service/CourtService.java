package com.example.TennisReservation.service;

import com.example.TennisReservation.model.Court;

import java.util.List;

public interface CourtService {

    /**
     * Creates new reservation.
     *
     * @param court which is created
     */
    void createCourt(Court court);

    /**
     *
     * @param id represents which court will be updated
     * @param court represents updated court
     */
    void updateCourt(long id, Court court);

    /**
     * Deletes selected court.
     *
     * @param id represents which court will be deleted
     */
    void deleteCourt(long id);

    /**
     * Gets all saved courts in the tennis club.
     *
     * @return list of all courts in the club
     */
    List<Court> getAllCourts();
}
