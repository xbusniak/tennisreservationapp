package com.example.TennisReservation.service;

import com.example.TennisReservation.model.Reservation;

import java.util.List;

public interface ReservationService {

    /**
     * Creates new reservation.
     *
     * @param reservation which is created
     * @return price for created reservation
     */
    double createReservation(Reservation reservation);


    /**
     * Updates selected reservation.
     *
     * @param id represents which reservation will be updated
     * @param reservation represents updated reservation
     */
    double updateReservation(long id, Reservation reservation);

    /**
     * Deletes selected reservation.
     *
     * @param id represents which reservation will be deleted
     */
    void deleteReservation(long id);

    /**
     * Gets all saved reservations in the tennis club.
     *
     * @return list of all reservations in the club
     */
    List<Reservation> getReservations();

    /**
     * Gets all reservations on given court.
     *
     * @param courtNumber represents court.
     * @return list of all the reservations on the given court.
     */
    List<Reservation> getReservationsByCourtNumber(int courtNumber);

    /**
     * Gets all the reservations for the given telephone number.
     *
     * @param telephoneNumber represents telephone number of one player
     * @return list of all reservations created by selected phone number
     */
    List<Reservation> getReservationByTelephoneNumber(String telephoneNumber);

    /**
     * Gets all the reservations for the given telephone number in the future.
     *
     * @param telephoneNumber represents telephone number of one player
     * @return list of all reservations created by selected phone number in future
     */
    List<Reservation> getReservationsByTelephoneNumberAndReservationDateTime(String telephoneNumber);

}
