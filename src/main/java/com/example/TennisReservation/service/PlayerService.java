package com.example.TennisReservation.service;

import com.example.TennisReservation.model.Player;

import java.util.List;

public interface PlayerService {

    /**
     * Creates new player.
     *
     * @param player which is created
     */
    void createPlayer(Player player);

    /**
     * Updates selected player.
     *
     * @param id represents which player will be updated
     * @param player represents updated player
     */
    void updatePlayer(long id, Player player);

    /**
     * Deletes selected player.
     *
     * @param id represents which player will be deleted
     */
    void deletePlayer(long id);

    /**
     * Gets all saved players in tennis club.
     *
     * @return list of all players in the club
     */
    List<Player> getAllPlayers();

}
