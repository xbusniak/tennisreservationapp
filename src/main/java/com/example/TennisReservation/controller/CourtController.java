package com.example.TennisReservation.controller;

import com.example.TennisReservation.model.Court;
import com.example.TennisReservation.service.CourtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CourtController {

    @Autowired
    private CourtService courtService;

    @GetMapping("/courts")
    public List<Court> getAllCourts() {
        return courtService.getAllCourts();
    }

    @PostMapping("/createCourt")
    public void createCourt(@RequestBody Court court) {
        courtService.createCourt(court);
    }

    @PostMapping("/updateCourt/{id}")
    public void updateCourt(@PathVariable long id, @RequestBody Court court) {
        courtService.updateCourt(id, court);
    }

    @DeleteMapping("/deleteCourt/{id}")
    public void deleteCourt(@PathVariable long id) {
        courtService.deleteCourt(id);
    }

}
