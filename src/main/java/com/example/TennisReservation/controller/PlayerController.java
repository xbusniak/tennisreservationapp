package com.example.TennisReservation.controller;

import com.example.TennisReservation.model.Player;
import com.example.TennisReservation.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @PostMapping("/createPlayer")
    public void createPlayer(@RequestBody Player player) {
        playerService.createPlayer(player);
    }

    @PostMapping("/updatePlayer/{id}")
    public void updatePlayer(@PathVariable long id,
                               @RequestBody Player player) {
        playerService.updatePlayer(id, player);
    }

    @DeleteMapping("/deletePlayer/{id}")
    public void deletePlayer(@PathVariable long id) {
        playerService.deletePlayer(id);
    }

    @GetMapping("/players/all")
    public List<Player> getAllPlayers() {
        return playerService.getAllPlayers();
    }

}
