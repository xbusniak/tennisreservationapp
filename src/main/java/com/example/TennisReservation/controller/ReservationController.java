package com.example.TennisReservation.controller;

import com.example.TennisReservation.model.Reservation;
import com.example.TennisReservation.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @GetMapping("/reservations/all")
    public List<Reservation> getAllReservations() {
        return reservationService.getReservations();
    }

    @GetMapping("/reservations/court/{courtNumber}")
    public List<Reservation> getReservationsByCourtNumber(@PathVariable int courtNumber) {
        return reservationService.getReservationsByCourtNumber(courtNumber);
    }

    @GetMapping("/reservations/telephoneNumber/{telephoneNumber}")
    public List<Reservation> getReservationByTelephoneNumber(@PathVariable String telephoneNumber) {
        return reservationService.getReservationByTelephoneNumber(telephoneNumber);
    }

    @GetMapping("/reservations/telephoneNumber/DateTime/{telephoneNumber}")
    public List<Reservation> getReservationsByTelephoneNumberAndReservationDateTime(@PathVariable String telephoneNumber) {
        return reservationService.getReservationsByTelephoneNumberAndReservationDateTime(telephoneNumber);
    }

    @PostMapping("/createReservation")
    public double createReservation(@RequestBody Reservation reservation) {
        return reservationService.createReservation(reservation);
    }

    @PostMapping("/updateReservation/{id}")
    public void updateReservation(@PathVariable long id,
                                    @RequestBody Reservation reservation) {
        reservationService.updateReservation(id, reservation);
    }

    @DeleteMapping("/deleteReservation/{id}")
    public void deleteReservation(@PathVariable long id) {
        reservationService.deleteReservation(id);
    }
}
